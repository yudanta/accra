#!/usr/bin/env python
from flask import (Blueprint, Flask, jsonify, redirect, render_template,
                   request, session, url_for)
from flask_cors import CORS

# APP Config
app = Flask(__name__, instance_relative_config=True)

# load config
app.config.from_object("config")
app.config.from_pyfile("application.cfg", silent=True)

# CORS
CORS(app, resources={r"/api/*": {"origins": "*"}})
# CORS(app)


@app.route("/", methods=["GET"])
def index():
    return "Video Conversion API"


# import api + register as blueprint
from app.api.v_0_1 import api, api_v_0_1

app.register_blueprint(api_v_0_1)
